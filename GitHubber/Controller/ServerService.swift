

import Foundation
import Alamofire

// класс для загрузки данных с сервера
class ServerService {
    var urlString = "https://api.github.com/users/" // url без логина
    
    // функция для запроса данных
    func getData(login: String, completion: (([Repos]?) -> Void)?) {
        let link = urlString + login + "/repos" // формируем итоговый url
        guard let url = URL(string: link) else {return}
        let request = URLRequest(url: url) // формируем реквест
        
        // делаем запрос
        AF.request(request).responseJSON { response in
            // если ошибка - передаем в комплишн nil
            guard let resp = response.response, resp.statusCode < 300 else {
                completion?(nil)
                return
            }
            // полученный непустой массив передаем в комплишн
            guard let data = response.data else {return}
            let reposArray = try! JSONDecoder().decode([Repos].self, from: data)
            completion?(reposArray)
        }
    }
}
