
import Foundation
// класс для управление данными между сервисами
class DataProvider {
    private let serverService = ServerService()
    private let dbService = DBService()
    // получить кэшированный массив
    func fetchReposArray(login: String, completion: (([ReposModel]?) -> Void)?){
        // получить данные с сервера
        serverService.getData(login: login, completion: { [weak self] reposArray in
            
            // если данных нет - передать nil
            guard let reposArray = reposArray else {
                completion?(nil)
                return
            }
            // кэшируем полученный массив
            self?.dbService.cache(reposArray)
            // передаем в комплишн полученный из БД массив
            let cachedObjects = self?.dbService.getObjects()
            
            DispatchQueue.main.async {
                completion?(cachedObjects)
            }
        })
    }
}
