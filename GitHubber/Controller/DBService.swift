
import Foundation
import RealmSwift

// класс для кэширования и запроса данных в БД
class DBService {
    let realm = try! Realm()

    // кэширование массива в БД
    func cache(_ reposArray: [Repos]) {
        
        try! realm.write {
            realm.deleteAll()
            for r in reposArray {
                let rep = ReposModel()
                rep.fullname = r.name
                rep.url = r.html_url
                realm.add(rep, update: .all)
            }
        }
    }
    // получение массива из БД
    func getObjects() -> [ReposModel]? {
        var reposArray = [ReposModel]()
        for r in realm.objects(ReposModel.self) {
            reposArray.append(r)
        }
        return reposArray
    }
}
