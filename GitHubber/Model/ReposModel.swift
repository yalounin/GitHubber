
import Foundation
import RealmSwift

// Realm-model
class ReposModel: Object {
    @objc dynamic var fullname = ""
    @objc dynamic var url = ""
    
    override class func primaryKey() -> String? {
        return "fullname"
    }
}
