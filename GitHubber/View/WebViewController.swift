

import UIKit
import WebKit

// vc для показа webView выбранного репозитория
class WebViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    var url = ""

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        // запрашиваем данные по url для отображения
        guard let urlLink = URL(string: url) else {return}
        let request = URLRequest(url: urlLink)
        webView.load(request)
    }
}
