

import UIKit

// вью контроллер с окном ввода имени пользователя
class LaunchViewController: UIViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        showAlert()
    }

    // показать окно ввода имени
    func showAlert() {
        let alertController = UIAlertController(title: "GitHub Login", message: "Введите имя пользователя", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Выбрать", style: .default, handler: {action in
            // инициализируем текстовое поле в алерт
            let textField = alertController.textFields?.first
            
            // если поле пустое или содержит пробелы или кириллицу, то перезапустить вью
            if (textField?.text == "" || textField?.text?.isCyrillic == true || textField?.text?.contains(" ") == true) {
                self.viewWillAppear(true)
                return
            }
                        
            // запустить вью контроллер для просмотра репозиториев введенного пользователя
            if let newLogin = textField?.text {
                guard let vc = self.storyboard?.instantiateViewController(identifier: "ViewController") as? ViewController else {return}
                vc.login = newLogin
                self.navigationController?.pushViewController(vc, animated: true)
            }
    })
        // добавляем textField
        alertController.addTextField(configurationHandler: {_ in})
                
        // добавляем кнопку в алерт
        alertController.addAction(saveAction)
        
        // показать алерт
        present(alertController, animated: true, completion: nil)
    
}

}

// проверка на кириллицу
extension String {
    var isCyrillic: Bool {
        let upper = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЮЯ"
        let lower = "абвгдежзийклмнопрстуфхцчшщьюя"

        for c in self.map({ String($0) }) {
            if !upper.contains(c) && !lower.contains(c) {
                return false
            }
        }
        return true
    }
}

