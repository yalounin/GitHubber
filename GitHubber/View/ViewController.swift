

import UIKit

// вью контроллер с показом репозиториев пользователя
class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
 
    @IBOutlet weak var tableView: UITableView! // таблица
    var repos: [ReposModel] = [] // массив для заполнения таблицы
    var provider = DataProvider() // провайдер, управляющий данными
    var login = "" // логин пользователя, введенный в предыдущем вью контроллере
    @IBOutlet weak var searchBar: UISearchBar! // поиск по таблице
    var filteredRepos = [ReposModel]() // массив для фильтрации
    
    // функция при показе вью
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = login // отображаем введенный текст в тайтле navigation controller
        
        // назначаем наблюдателей клавиатуры для сдвига вью перед вводом текста
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        // получаем данные через провайдер
        provider.fetchReposArray(login: login, completion: { reposArray in
            // если полученных данных нет, то отобразить ошибку
            guard let reposArray = reposArray else {
                let alertController = UIAlertController(title: "Ошибка", message: nil, preferredStyle: .alert)
                let action = UIAlertAction(title: "Ок", style: .default, handler: { _ in
                    self.navigationController?.popViewController(animated: true)
                }) // закрыть vc при нажатии на "Ок"
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)
                return
            }
            self.repos = reposArray // присвоить полученный провайдером массив
            self.filteredRepos = reposArray // создаем дубль массива для фильтрации
            self.searchBar.text = ""
            self.searchBar.delegate = self
            self.tableView.reloadData() // перезагружаем таблицу
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repos.count
    }
    
    // инициализация строк таблицы
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let c = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCell
        c.title.text = repos[indexPath.row].fullname // отображаем название репозитория в тайтле строки
        return c
    }
    
    // row selection
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // запускаем другой вью контроллер для показа web view данного репозитория
        guard let vc = storyboard?.instantiateViewController(identifier: "WebViewController") as? WebViewController else {return}
        
        vc.url = repos[indexPath.row].url // передаем url
        
        self.navigationController?.pushViewController(vc, animated: true) // отображаем vc

    }
    
    // строка поиска
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        // если строка пустая (вариант при стирании символов) - то вернуть обратно первоначальный массив
        if searchBar.text?.count == 0 {
            repos = filteredRepos
        } else {
            // фильтруем относительно введенного текста
            repos = filteredRepos.filter() {
                $0.fullname.lowercased().contains(searchText.lowercased())
            }
        }
        tableView.reloadData()
    }
    
    // функция для скрытия клавиатуры поиска по кнопке Search
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    // отобразить клавиатуру со сдвигом вью
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tableView.contentInset.bottom = keyboardHeight // поднимаем нижний край вью до верхней границы клавиатуры
        }
    }
    // возвращаем нижний край вью на дефолтное значение
    @objc func keyboardWillHide(notification: NSNotification) {
        tableView.contentInset.bottom = 0.0
    }
}

