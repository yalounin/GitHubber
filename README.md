Проект "GitHubber"

Цель: ввод логина GitHub и просмотр всех репозиториев аккаунта с возможностью фильтрации. При нажатии на репозиторий происходит переход на адаптивную версию сайта через WebView.

API: https://docs.github.com/en/rest 

Stack:
Swift
UIKit
Alamofire
Realm
Codable
WebView


Architecture: Model-View-Controller
